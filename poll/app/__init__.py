from email.message import Message
from flask import Flask, render_template, redirect, url_for, request
from json import load, dump
from os import path


app = Flask(__name__)


def update_votes(votes):
    with open("votes.json", "w+") as vote_file:
        dump(votes, vote_file)
    return


# Load votes
if path.exists("votes.json"):
    with open("votes.json") as vote_file:
        votes = load(vote_file)
else:
    votes = {}


@app.route("/<message>")
@app.route("/", defaults={"message": ""})
def index(message):
    return render_template("poll.html", votes=votes, message=message)


@app.route("/overview")
def overview():
    return render_template("overview.html", votes=votes)


@app.route("/add-option", methods=["POST"])
def add_option():
    option = request.form.get("option")
    if option is not None:
        votes[option] = 0

    update_votes(votes)

    return redirect(url_for("overview"))


@app.route("/reset/<option>", methods=["POST"])
def reset_option(option):
    if option in votes:
        votes[option] = 0


@app.route("/vote", methods=["POST"])
def vote():
    option = request.form.get("option")
    if option is not None:
        votes[option] += 1
        update_votes(votes)
        return redirect(url_for("index", message="Danke für's Abstimmen!"))
    else:
        return redirect(url_for("index"))



from app import app
from flask import render_template, redirect, request
from json import dump


@app.route("/")
def index():
    return render_template("index.html", threads=app.threads)


@app.route("/thread/<int:id>", methods=["GET", "POST"])
def view_thread(id):
    if request.method == "GET":
        if id in app.threads:
            return render_template("thread.html", thread=app.threads[id])
        else:
            return redirect("/")
    elif request.method == "POST":
        if "answer" in request.form and id in app.threads:
            # Append new answer to answer list
            app.threads[id]["answers"].append(request.form["answer"])

            # Dump to file
            with open("threads.json", "w") as f:
                dump(app.threads, f)

            return redirect(f"/thread/{id}")
        else:
            return redirect("/create", code=401)


@app.route("/create", methods=["POST", "GET"])
def create_thread():
    if request.method == "GET":
        return render_template("create.html")
    elif request.method == "POST":
        if "name" in request.form and "description" in request.form:
            # Evaluate ID and save to internal app.threads
            id = len(app.threads) + 1
            app.threads[id] = {
                "name": request.form["name"],
                "description": request.form["description"],
                "answers": [],
            }

            # Dump to file
            with open("threads.json", "w") as f:
                dump(app.threads, f)

            return view_thread(id)
        else:
            return redirect("/create", code=401)

from flask import Flask
from json import load

# Create new Flask instance and empty threads
app = Flask(__name__)
app.threads = {}
# ^ Structure id: {"name": ..., "description": ... "answers": [""...]}

# Load files gracefully
try:
    with open("threads.json") as f:
        for key, val in load(f).items():
            # This ensure keys are inserted as int because
            # JSON doesn't support int keys nativly
            app.threads[int(key)] = val
except Exception as e:
    app.threads = {}


from .routes import *

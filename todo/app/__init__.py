from flask import Flask, render_template, url_for, redirect, request

# Initialize Flask instance
app = Flask(__name__)

# Create empty todo
todos = {"First item": "black"}


@app.route("/")
def index():
    return render_template("index.html", todos=todos)


@app.route("/remove/todo")
def check_todo(todo):
    return redirect(url_for("index"))


@app.route("/add", methods=["POST"])
def add_todo():
    new_todo, color = request.form.get("todo"), request.form.get("color")
    print(color)
    if new_todo is not None or color is not None:
        todos[new_todo] = color
    return redirect(url_for("index"))

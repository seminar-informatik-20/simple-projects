from flask import Flask, render_template, redirect, request
from .helper import load_messages, update_messages

app = Flask(__name__)
messages = load_messages()


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/contact-me", methods=["POST", "GET"])
def contact_me():
    if request.method == "GET":
        return render_template("contact-me.html", message="")
    elif request.method == "POST":
        subject, message, e_mail = (
            request.form.get("subject"),
            request.form.get("message"),
            request.form.get("e_mail"),
        )
        if None not in [subject, message, e_mail]:
            messages[subject] = message
            update_messages(messages)
            return render_template(
                "contact-me.html",
                message="Message is sent. Your answer may take some time.",
            )
        else:
            return redirect(url_for("index"))

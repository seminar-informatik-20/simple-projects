from json import load, dump
from os import path


def update_messages(messages):
    with open("messages.json", "w+") as message_file:
        dump(messages, message_file)


def load_messages():
    if path.exists("messages.json"):
        with open("messages.json", "r") as message_file:
            return load(message_file)
    else:
        with open("messages.json", "w+") as message_file:
            message_file.write("{}")
        return {}

